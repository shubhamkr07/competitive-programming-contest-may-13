from collections import Counter

def isSubSet(subset, superset):
    count = 0
    for i in subset:
        if i in superset and subset[i] <= superset[i]:
            count = 1
        else:
            count = 0
            break
    return count

with open('input.txt') as f:
    lines = f.readlines()

lines = [x.strip() for x in lines   ] 
lines[:] = [item for item in lines if item != '']
lineLength = len(lines)
line = 0

f = open('output.txt', 'a+')

while line < lineLength:
    t = int(lines[line])
    line += 1
    for i in range(t):
        s = lines[line].split(' ')
        ls = []
        line += 1
        N = int(s[0])
        D = s[1:N+1]
        W = ''.join(sorted(''.join(s[N+1:])))
        countW = dict(Counter(W))
        for word in D:
            countC = dict(Counter(word))
            if isSubSet(countC, countW) == 1:
                ls.append(word)
        ls.sort()
        f.write(' '.join(ls) + '\n')
f.write('\n')
f.close()

