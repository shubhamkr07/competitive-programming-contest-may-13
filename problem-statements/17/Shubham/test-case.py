import random, string
from random import randint
testcase = int(input())
T = randint(1, testcase)
f = open('input.txt', 'a+')
f.write(str(T) + '\n')
for i in range(T):
    ls = []
    N = randint(1, testcase)
    ls.append(str(N))
    for j in range(N):
        L = randint(2,10)
        sub = ''
        for k in range(L):
            sub += random.choice(string.ascii_lowercase)
        ls.append(sub)
    sen = ''
    for k in range(1, N):
        S1 = randint(1,N)
        sen += random.choice(string.ascii_lowercase)+random.choice(string.ascii_lowercase)+''.join(ls[S1:S1+1])+random.choice(string.ascii_lowercase)+random.choice(string.ascii_lowercase)
    ls.append(sen)
    f.write(' '.join(ls) + '\n')
f.write('\n')
f.close()